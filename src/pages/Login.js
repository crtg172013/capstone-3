import { useState, useEffect, useContext } from 'react';
import { Form, Button, FloatingLabel,Nav} from 'react-bootstrap';
import { Navigate ,NavLink } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext'

export default function Login() {

    const { user, setUser } = useContext(UserContext);

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [isActive, setIsActive] = useState(false);

    function authenticate(e) {

        e.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)

            if(data.access !== undefined){
                localStorage.setItem('token', data.access);
                retrieveUserDetails(data.access);

                Swal.fire({
                    title: "Login Successful",
                    icon: "success",
                    text: "Login successful!"
                });
            } else {
                Swal.fire({
                    title: "Authentication failed",
                    icon: "error",
                    text: "Check your login details and try again!"
                })
            }
        })

        setEmail('');
        setPassword('');
    }


    // Retrieving user details

    const retrieveUserDetails = (token) => {

        fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            setUser({
                id: data._id,
                isAdmin: data.isAdmin
            })
        })
    }

    useEffect(() => {

        if ((email !== '' && password !== '')) {
            setIsActive(true);
        } else {
            setIsActive(false);
        }

        }, [email, password]);

    return (
        (user.isAdmin === true)
        ?
            <Navigate to='/admin'/>
        :
            (user.id !== null)
            ?
                <Navigate to='/products'/>
            :
        
        <Form className = "col-lg-4 col-md-4 col-sm-12 login-form" onSubmit={(e) => authenticate(e)}>
            <h1 className = "text-white text-center">Log in User </h1>
            <Form.Group controlId = "userEmail">
                <FloatingLabel
                             controlId="floatingEmail"
                               label="Email Address"
                               className="mb-3">
                <Form.Control 
                    type="email"
                    placeholder="Email Address"
                    required
                    value={email}
                    onChange={e => setEmail(e.target.value)}
                />
                </FloatingLabel>
            </Form.Group>

            <Form.Group className="mb-3" controlId = "password">
                <FloatingLabel
                             controlId="floatingPassword1"
                               label="Password"
                               className="mb-3">
                <Form.Control                   
                    type="password"
                    placeholder="Password"
                    required
                    value={password}
                    onChange={e => setPassword(e.target.value)}
                />
                </FloatingLabel>
            </Form.Group>       
            {isActive
                ?
                    <Button variant="danger" type="submit" id="submitBtn">Login</Button>
                :   <Button variant="success" type="submit" id="submitBtn" disabled>Login</Button>
            }

            <p className = "text-center text-white">Don't have account?</p>
             <Nav.Link className = "text-center text-white" as={NavLink} to="/register"><u>Sign Up</u></Nav.Link>
        </Form>
    )
}