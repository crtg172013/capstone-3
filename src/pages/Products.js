import { useEffect, useState } from 'react';
import ProductCard from '../components/ProductCard';
import {Carousel,Card, Col, Row	} from 'react-bootstrap';

export default function Products() {

	// State that will be used to store the products retrieved from the database
	const [products, setProducts ] = useState([]);

	// Retrieves the products from the database upon initial render of the "Products" component

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products`)
		.then(res => res.json())
		.then(data => {
			console.log(data);

			// Sets the "products" state to map the data retrieved from the fetch request into several "ProductCard" components
			setProducts(data.map(product => {
				return (
					<ProductCard key={product._id} productProp= {product}/>
				)
			}))
		})
	}, [])

	return (
		<>
			<h1 className = "text-white">Products</h1>	
{/*Caroussel*/}
			<Carousel fade className = "productCarrousel fluid">
			<Carousel.Item>
			        <img
			          className="product-image img-fluid"
			          src="https://www.wallpapertip.com/wmimgs/5-54294_white-persian-cat-wallpaper-hd.jpg"
			          alt=""
			        />
			        <Carousel.Caption>
			          <p className	="text-white justify-content-center m-0 p-0" style={{fontSize: 50, background: "#0C3C80 "}}><b>Persian Cat</b></p>
			        </Carousel.Caption>
			      </Carousel.Item>
			      <Carousel.Item>
			        <img
			          className="product-image img-fluid"
			          src="https://wallpaperaccess.com/full/1807868.jpg"
			          alt=""
			        />
			        <Carousel.Caption>
			          <h3 className	="text-white justify-content-center m-0 p-0" style={{fontSize: 50, background: "#0C3C80 "}}>Siamese Cat</h3>
			        </Carousel.Caption>
			      </Carousel.Item>
			      <Carousel.Item>
			        <img
			          className="product-image img-fluid"
			          src="https://petcircle.com.au/petcircle-assets/images/blog/breed-of-the-month-the-sphynx-hero.jpg"
			          alt="First slide"
			        />
			        <Carousel.Caption>
			          <h3 className	="text-white justify-content-center m-0 p-0" style={{fontSize: 50, background: "#0C3C80 "}}>Sphynx Cat</h3>
			        </Carousel.Caption>
			      </Carousel.Item>
			      <Carousel.Item>
			        <img
			          className="product-image img-fluid"
			          src="https://as2.ftcdn.net/v2/jpg/00/23/64/21/1000_F_23642119_PIjCYMElZhLoCdMbeAo8QMqp2Ii7OIMe.jpg"
			          alt="Second slide"
			        />

			        <Carousel.Caption>
			          <h3 className	="text-white justify-content-center m-0 p-0" style={{fontSize: 50, background: "#0C3C80 "}}>British ShortHair</h3>
			        </Carousel.Caption>
			      </Carousel.Item>
			      <Carousel.Item>
			        <img
			          className="product-image img-fluid"
			          src="https://as2.ftcdn.net/v2/jpg/00/40/85/65/1000_F_40856593_1G9TGy3zR0WjAACobDUQaNDY86YaIW0A.jpg"
			          alt="Third slide"
			        />

			        <Carousel.Caption>
			          <h3 className	="text-white justify-content-center m-0 p-0" style={{fontSize: 50, background: "#0C3C80 "}}>Ragdoll Cat</h3>
			         </Carousel.Caption>
			      </Carousel.Item>
			      <Carousel.Item>
			        <img
			          className="product-image img-fluid "

			          src="https://img4.goodfon.com/wallpaper/big/9/b9/shotlandskaia-visloukhaia-koshka-mordochka-vzgliad-koshka.jpg"
			          alt="Fourth slide"
			        />
			        <Carousel.Caption>
			          <h3 className	="text-white justify-content-center m-0 p-0" style={{fontSize: 50, background: "#0C3C80 "}}>Scottish Fold</h3>
			         </Carousel.Caption>
			      </Carousel.Item>
			       <Carousel.Item>
			        <img
			          className="product-image img-fluid "

			          src="https://petkeen.com/wp-content/uploads/2022/05/Munchkin-Cat_MDavidova_Shutterstock-760x507.jpg"
			          alt="Fifth slide"
			        />
			        <Carousel.Caption>
			          <h3 className	="text-white justify-content-center m-0 p-0" style={{fontSize: 50, background: "#0C3C80 "}}>Munchkin Cat</h3>
			         </Carousel.Caption>
			      </Carousel.Item>
			      <Carousel.Item	>
			      <img
			          className="product-image img-fluid "

			          src="https://thumbs.dreamstime.com/b/brown-exotic-shorthair-kitten-lying-window-looks-up-toddler-animals-persian-cats-concept-adorable-159856886.jpg"
			          alt="sixth slide"
			        />
			        <Carousel.Caption>
			          <h3 className	="text-white justify-content-center m-0 p-0" style={{fontSize: 50, background: "#0C3C80 "}}>Exotic ShortHair Cat</h3>
			         </Carousel.Caption>
			      </Carousel.Item>
			      <Carousel.Item	>
			      <img
			          className="product-image img-fluid "

			          src="https://as1.ftcdn.net/v2/jpg/00/36/16/36/1000_F_36163613_tGc9KdEbpPAqMIZo9RmgdFyz1oqylsqV.jpg"
			          alt="seventh slide"
			        />
			        <Carousel.Caption>
			          <h3 className	="text-white justify-content-center m-0 p-0" style={{fontSize: 50, background: "#0C3C80 "}}>American ShortHair Cat</h3>
			         </Carousel.Caption>
			      </Carousel.Item>
			      
			    </Carousel>
{/*End of Caroussel*/}

			<Row xs={1} md={2} className="g-4">{products}</Row>
		</>
	)
}